import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  collection;


  constructor(
    private db: AngularFirestore
  ) { }

  registraConta(conta){
    const id = this.db.createId();
    this.collection = this.db.collection('conta');
    this.collection.doc(id).set(conta);
  }
}
