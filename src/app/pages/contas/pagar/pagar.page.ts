import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  contasForm: FormGroup;

  constructor(
    private builder: FormBuilder,    
    private nav: NavController
  ) { }

  ngOnInit() {
    this.initForm();
  }



  private initForm(){
    this.contasForm = this.builder.group({
      valor: ['',[Validators.required, Validators.min(0.0)]],
      parceiro: ['', [Validators.required, Validators.minLength(5)]],
      descricao: ['',[Validators.required, Validators.minLength(6)]],
    });
  }
  /**
   * Salva a nova conta dentro do Firebase
   */
  registraConta(){

  }
}
